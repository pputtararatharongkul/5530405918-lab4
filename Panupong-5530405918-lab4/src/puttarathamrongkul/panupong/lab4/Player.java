package puttarathamrongkul.panupong.lab4;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab4
 * This program to learn about Java OOP.
 */
import java.util.Calendar;
import java.util.GregorianCalendar;
public class Player {
	protected String name;
	protected int bDay, bMonth, bYear;
	protected double weight, height;
	public Player(String n, int bD, int bM, int bY, double w, double h) {
		name = n;
		bDay = bD;
		bMonth = bM;
		bYear = bY;
		weight = w;
		height = h;
	}
	public String getName(){
		return name;
	}
	public double getbDay(){
		return bDay;
	}
	public double getbMonth(){
		return bMonth;
	}
	public double getbYear(){
		return bYear;
	}
	public double getWeight(){
		return weight;
	}
	public double getHeight(){
		return height;
	}
	public void setWeight(int newWeight){
		this.weight = newWeight;
	}
	
	public int res() {
	    Calendar cal = new GregorianCalendar(bYear, bMonth, bDay);
	    Calendar now = new GregorianCalendar();
	    int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
	    if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
	        || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal.get(Calendar.DAY_OF_MONTH) > now
	            .get(Calendar.DAY_OF_MONTH))) {
	      res--;
	    }
	    return res;
	  }
	public String toString(){
		return ("Player [" + getName() + " is " + res() + " year old, with weight = " + getWeight() + " and height = " + getHeight() + "]");
	}
}