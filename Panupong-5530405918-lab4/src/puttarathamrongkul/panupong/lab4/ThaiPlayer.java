package puttarathamrongkul.panupong.lab4;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab4
 * This program to learn about Java OOP.
 */
public class ThaiPlayer extends Player {
	public ThaiPlayer(String n, int bD, int bM, int bY, double w, double h) {
		super(n, bD, bM, bY, w, h);
	}

	private String sportname;
	private String nationality = "Thai";

	public void Player(String sn, String nat) {
		this.sportname = sn;
		this.nationality = nat;
	}

	public String getSportName() {
		return sportname;
	}

	public String getNationality() {
		return nationality;
	}

	public void setSportName(String newsportname) {
		this.sportname = newsportname;
	}

	public String toString() {
		if (sportname != null)
			return ("Player [" + getName() + " is " + res()
					+ " year old, with weight = " + getWeight()
					+ " and height = " + getHeight() + "]\n[nationality = "
					+ getNationality() + " award = " + getSportName() + "]");
		else
			return ("Player [" + getName() + " is " + res()
					+ " year old, with weight = " + getWeight()
					+ " and height = " + getHeight() + "]\n[nationality = "
					+ getNationality() + "]");
	}
}